const { parseMultipartData, sanitizeEntity } = require('strapi-utils');
module.exports = {
    async create(ctx) {
        let entity;
        if (ctx.is('multipart')) {
            const { data, files } = parseMultipartData(ctx);
            entity = await strapi.services.credenciamento.create(data, { files });
        } else {
            entity = await strapi.services.credenciamento.create(ctx.request.body);
        }

        entry = sanitizeEntity(entity, { model: strapi.models.credenciamento });
        await strapi.plugins['email'].services.email.send({
            to: 'eudangeld@gmail.com',
            // to: 'glauce@ms2comunicacao.com.br',
            from: 'credenciamento@siteendurance.com.br',
            subject: 'Novo credenciamento',
            text: `
          Uma nova mensagem foi enviada através do formulário de credenciamento de imprensa.<br>
          <strong>Nome:</strong>      ${entry.name}     <br>
          <strong>Veículo:</strong>   ${entry.veiculo}  <br>
          <strong>Cargo:</strong>     ${entry.cargo}    <br>
          <strong>Email:</strong>     ${entry.email}    <br>
          <strong>Telefone:</strong>  ${entry.telefone} <br>
        `,
        });
        return entry;
    },
};